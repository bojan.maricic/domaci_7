<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Car Dealer | Registracija</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<meta property="og:title" content="Car Dealer"/>
	<meta property="og:description" content="Novi i polovni automobili" />
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>
	<link rel="stylesheet" href="css/font-awesome/font-awesome.min.css" media="screen"/>
	<link rel="stylesheet" href="css/flaticon/flaticon.css" media="screen"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="js/javascript.js"></script>

	</head>
	
	<body>
		<div class="wrapper">
		
			<!--HEADER-->
			<?php include ('header.html') ?>
			<!--KRAJ HEADER-->
			
			<!--CONTAINER-->
			<div class="container">
							
				<!--REGISTRACIJA-->
				<div class="registracija">
					<div class="title">
					<h2>Registracija</h2>		
					<p>Registraciona prijava</p>	
					</div>	
					<?php
			        if(isset($_GET["message"])) { 
			          echo "<div class=\"message\">".$_GET["message"]."</div>\n";
			        }
			        ?>  
					<div class="registracija-box">
						<h4>Sva polja označena sa <span>*</span> morate obavezno da popunite</h4>
							<form action="register.php" method="post">
							
										<p>Ime i prezime <span>*</span></p>
											<input type="text" name="name" value=""  oninvalid="this.setCustomValidity('Unesite vaše ime i prezime')" oninput="setCustomValidity('')" />
													
										<p>E mail <span>*</span><p>	
											<input type="email" name="email" value=""  oninvalid="this.setCustomValidity('Unesite vaš e-mail')" oninput="setCustomValidity('')" />
											
										<p>Datum rodjenja <span>*</span><p>
											<input type="date" name="birthday" oninvalid="this.setCustomValidity('Unesite vaš datum rođenja')" oninput="setCustomValidity('')" />

										<p>Telefon<p>
											<input type="tel" name="phone" value="">

										<p>Adresa stanovanja<p>
											<input type="text" name="address">
											
										<p>Mesto stanovanja</p>
											<input type="text" name="city">
						
					
										<button type="submit" name="submit" value="" />Pošalji</button>
							</form>
					</div>
				</div>	
				<!--KRAJ REGISTRACIJE-->
				
				
			</div>
			<!--KRAJ SIDEBAR-->
			
				<!--FOOTER-->
			<?php include ('footer.html') ?>
				<!--KRAJ FOOTER-->
		</div>
	</body>
</html>