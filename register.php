<!DOCTYPE html">
<html>
	<head>
	<meta charset="utf-8">
	<title>Registracija</title>	
	<meta name="description" content="Dobro došli na našu web stranicu. Ovde možete pogledati našu ponudu novih i polovnih automobila različitih proizvođača.">
	<meta name="keywords" content="#">
	<meta name="author" content="Bojan">
	<meta name="viewport" content="width=device-width, initial-scale=1.O">	
	<meta property="og:title" content="Car Dealer"/>
	<meta property="og:description" content="Novi i polovni automobili" />
	<link href="https://fonts.googleapis.com/css?family=Kalam:300,400,700|Oswald:300,400,500,600,700|Roboto+Condensed:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="css/normalize.css">
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="css/responsive.css" media="all"/>




	</head>
	
	<body>
		<div class="wrapper">
			
			<!--CONTAINER-->
			<div class="container">
							

				<div class="registracija">
					<div class="title">
						<h2>Registracija</h2>		
						<p>Registraciona prijava</p>	
					</div>	
					<div class="registracija-box">
						<h4>Vaši podaci iz registracione prijave su:</h4>

<?php
if(isset($_POST['submit'])) {

    $errors = 0;

    // validation
    if(isset($_POST['name']) && $_POST['name']!='') {
      $name = filter_var($_POST['name'], FILTER_SANITIZE_STRING);
    } else {
      $errors = 1;
      $message = "Neispravno ime i prezime";
    }

    
    if(isset($_POST['email']) && $_POST['email']!='') {
      $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
	
    } else {
      $errors = 1;
      $message = "Neispravan email";
    }

    if(isset($_POST['birthday']) && $_POST['birthday']!='') {
      $birthday = $_POST['birthday'];
    } else {
      $errors = 1;
      $message = "Neispravan datum rođenja";
    }

	$phone = $_POST['phone'];
    $address = $_POST['address'];
    $city = $_POST['city'];


    if($errors === 0) {

      echo "
      Ime i prezime: ".$name."<br>\n
      E-mail: ".$email."<br>\n
      Telefon: ".$phone."<br>\n
      Adresa stanovanja: ".$address."<br>\n
      Mesto stanovanja: ".$city."<br><br>\n
      Datum rođenja: ".$birthday."<br>\n";

      echo "Vi ste rođeni: " . date("l", strtotime($birthday));

 } else {

      header('Location: registracija.php?message='.$message.'');

    }
  
} 
?>

					</div>
				</div>	
			</div>
	</body>
</html>